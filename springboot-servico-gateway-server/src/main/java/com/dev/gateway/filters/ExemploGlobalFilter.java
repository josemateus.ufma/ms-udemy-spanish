package com.dev.gateway.filters;

import java.util.Optional;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
//import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class ExemploGlobalFilter implements GlobalFilter, Ordered {

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		System.out.println("Executando filtro pre-request");
		exchange.getRequest().mutate().headers( h -> h.add("token", "1234567890"));

		return chain.filter(exchange).then(Mono.fromRunnable(() -> { // o que está aqui dentro é o Post request
			System.out.println("Executando filtro post-request");
			exchange.getResponse().getCookies().add("ChaveQualquer",
					ResponseCookie.from("ChaveQualquer", "valorQualquer").build());
			
//			exchange.getResponse().getHeaders().setContentType(MediaType.TEXT_PLAIN);
			
			Optional.ofNullable(exchange.getRequest().getHeaders().getFirst("token")).ifPresent(valor -> {
				exchange.getResponse().getHeaders().add("token", valor + "RESPOSTA");
			});
		}));
	}

	@Override
	public int getOrder() {
		return 1;
	}

}
