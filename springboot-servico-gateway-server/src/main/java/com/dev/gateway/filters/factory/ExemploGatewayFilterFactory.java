package com.dev.gateway.filters.factory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class ExemploGatewayFilterFactory
		extends AbstractGatewayFilterFactory<ExemploGatewayFilterFactory.Configuracao> {

	
	public ExemploGatewayFilterFactory() {
		super(Configuracao.class);
	}
	
	
	@Override
	public String name() {
		return "ExemploCookieNome";
	}
	
	
	@Override
	public List<String> shortcutFieldOrder() {
		return Arrays.asList("mensagem", "nomeCookie", "valorCookie");
	}

	@Override
	public GatewayFilter apply(Configuracao config) {

		System.out.println("Executando o gateway pre filter factory " + config.mensagem);

		return new OrderedGatewayFilter((exchange, chain) -> {
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {

				Optional.ofNullable(config.valorCookie).ifPresent(cookie -> {
					exchange.getResponse().addCookie(ResponseCookie.from(config.nomeCookie, cookie).build());
				});

				System.out.println("Executando o gateway post filter factory " + config.mensagem);

			}));
		}, 2);

	}

	public static class Configuracao {

		private String mensagem;
		private String nomeCookie;
		private String valorCookie;

		public String getMensagem() {
			return mensagem;
		}

		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}

		public String getNomeCookie() {
			return nomeCookie;
		}

		public void setNomeCookie(String nomeCookie) {
			this.nomeCookie = nomeCookie;
		}

		public String getValorCookie() {
			return valorCookie;
		}

		public void setValorCookie(String valorCookie) {
			this.valorCookie = valorCookie;
		}

	}

}
