package com.dev.items.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.dev.items.models.Produto;

@FeignClient(name = "servico-produtos") //nome do ms que quero conectar
public interface ProdutoClienteRest {
	
	@GetMapping("/listar")
	public List<Produto> listar();
	
	@GetMapping("/ver/{id}")
	public Produto detalhar(@PathVariable Long id);

}
