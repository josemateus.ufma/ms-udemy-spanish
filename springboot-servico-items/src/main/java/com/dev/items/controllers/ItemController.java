package com.dev.items.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dev.items.models.Item;
import com.dev.items.models.Produto;
import com.dev.items.models.service.ItemService;

@RestController
public class ItemController {
	
	@Autowired
	@Qualifier("serviceFeign")
	private ItemService itemService;
	
	
	@Autowired
	private CircuitBreakerFactory cbFactory;
	
	
	@GetMapping("/listar")
	public List<Item> listar(@RequestParam(name = "nome", required = false) String nome, 
							@RequestHeader(name="token-request", required = false) String token){
		System.out.println(nome);
		System.out.println(token);
		return itemService.findAll();
	}
	
	@GetMapping("/ver/{id}/quantidade/{quantidade}")
	public Item detalhar(@PathVariable Long id, @PathVariable Integer quantidade) {
		return cbFactory.create("items")
				.run(() -> itemService.findById(id, quantidade), 
						e -> metodoAlternativo(id, quantidade, e));
	}

	private Item metodoAlternativo(Long id, Integer quantidade, Throwable e) {
		System.out.println(e.getMessage());
		Item item = new Item();
		Produto produto = new Produto();
		
		item.setQuantidade(0);
		produto.setId(0L);
		produto.setNome("DEU ERRO");
		produto.setPreco(0.00);
		item.setProduto(produto);
		
		return item;
	}

}
