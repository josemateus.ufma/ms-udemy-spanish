package com.dev.items.models.service;

import java.util.List;

import com.dev.items.models.Item;

public interface ItemService {
	
	public List<Item> findAll();
	public Item findById(Long id, Integer quantidade);

}
