package com.dev.produtos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringbootServicoProdutosApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicoProdutosApplication.class, args);
	}

}
