package com.dev.produtos.controllers;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dev.produtos.models.entity.Produto;
import com.dev.produtos.models.service.IProdutoService;

@RestController
public class ProdutoController {
	
	@Autowired
	private IProdutoService produtoService;
	
	@Autowired
	private Environment env;
	
//	@Value("${server.port}")
//	private Integer port;
	
	
	@GetMapping("/listar")
	public List<Produto> listar() {
		
		return produtoService.findAll().stream().map(produto ->{
			produto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			return produto;
		}).collect(Collectors.toList());
		
		
	}
	
	@GetMapping("/ver/{id}")
	public Produto detalhar(@PathVariable Long id) throws InterruptedException {
		
		if (id.equals(10L)) {
			throw new IllegalStateException("Mensagem de erro forçada: Produto não encontrado!");
		}
		
		if(id.equals(7L)) {
			TimeUnit.SECONDS.sleep(5);
		}
		
		Produto p = produtoService.findById(id);
//		p.setPort(port);
		p.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		return p;
	}

}
