package com.dev.produtos.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dev.produtos.models.entity.Produto;

@Repository
public interface ProdutoDao extends CrudRepository<Produto, Long>{

}
