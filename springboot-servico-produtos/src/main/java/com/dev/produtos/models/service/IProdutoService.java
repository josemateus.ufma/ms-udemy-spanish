package com.dev.produtos.models.service;

import java.util.List;

import com.dev.produtos.models.entity.Produto;

public interface IProdutoService {
	
	public List<Produto> findAll();
	public Produto findById(Long id);

}
