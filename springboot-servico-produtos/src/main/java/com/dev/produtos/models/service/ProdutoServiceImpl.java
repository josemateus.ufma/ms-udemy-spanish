package com.dev.produtos.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.produtos.models.dao.ProdutoDao;
import com.dev.produtos.models.entity.Produto;

@Service
public class ProdutoServiceImpl implements IProdutoService{
	
	@Autowired
	private ProdutoDao produtoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Produto> findAll() {
		return (List<Produto>) produtoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Produto findById(Long id) {
		return produtoDao.findById(id).orElse(null);
	}

}
