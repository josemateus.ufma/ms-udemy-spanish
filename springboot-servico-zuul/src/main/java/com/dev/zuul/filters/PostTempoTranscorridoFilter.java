package com.dev.zuul.filters;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class PostTempoTranscorridoFilter extends ZuulFilter{
	
	private static Logger log = LoggerFactory.getLogger(PostTempoTranscorridoFilter.class);

	@Override
	public boolean shouldFilter() { // deve executar esse filtro??
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		log.info("Entrando no filtro pós request");
		Long tInicio = (Long) request.getAttribute("tempoInicio");
		Long tFinal = System.currentTimeMillis();
		Long tempo = (tFinal - tInicio);
		System.out.println("Tempo da request em ms: " + tempo/1000.0);
		
		return null;
	}

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 2;
	}

}
