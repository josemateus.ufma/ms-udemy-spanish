package com.dev.zuul.filters;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class PreTempoTranscorridoFilter extends ZuulFilter{
	
	private static Logger log = LoggerFactory.getLogger(PreTempoTranscorridoFilter.class);

	@Override
	public boolean shouldFilter() { // deve executar esse filtro??
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		log.info(String.format("%s request chamada para %s", request.getMethod(), request.getRequestURI().toString()));
		
		
		Long tInicio =  System.currentTimeMillis();
		request.setAttribute("tempoInicio", tInicio);
		
		return null;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

}
